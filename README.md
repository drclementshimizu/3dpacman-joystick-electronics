# 3DPacman-Joystick-Electronics

Electronics design by Dr. Clement Shimizu of www.Elumenati.com for Keita 
Takahshi. The parts list will fit inside the 3D printed housing designed by
Trevor McDonald.

PAC-MAN was designed by Toru Iwatani is property of NAMCO.  3D PAC-MAN was 
created by Dr. Clement Shimizu for Keita Takahshi in collabration with the
Elumenati. 3D PAC-MAN was shown at the Babycastles Summit at Musem of Arts and 
Design in Manhattan and Pulse ART + TECHNOLOGY FESTIVAL in Savannah.

Parts list for wireless arcade joystick controllers:
* Arduino Pro Micro ATmega32U4 5V/16MHz Module Board 
* NRF24L01+ 2.4GHz Wireless RF Transceiver
* Socket Adapter Board For 8PIN NRF24L01 Wireless Module
* 0.9V-5V to 5V Boost module
* 1.5V AA Battery Holder(for two batteries)
* knobby arcade joystick (Most are arcade joysticks are 8 way but some have an 
  adapter plate for switching to 4-way mode for games where you dont move in 
  diagonal lines).
* Light up arcade button.  (1 1/8 inch diameter).  The kind that has the screw 
  on back nut works better than the ones that are snap in, make sure the LED
  supply voltage is 5 volts)
* Jumper wires.  You can get ribbon cable style jumpers with male and or female
  ends that make prototyping really easy
* other basics like soldiering iron and solder.

Please note the svg wiring diagram has embeded images that may not show up in
the embeded preview, so its best to download the svg file and THEN view it in
Chrome.

